#!/usr/bin/env python2


"""

        This file parse the raw data contained in files.

        It can be adjacency matrices or the list of the edges.

"""


import sys
import os


class Data(object):
    """Data class.

    Basic data class representing data from file.

    """

    def __init__(self):
        """self-explanatory"""

        self.mytype = None
        self.rawdata = []
        self.edges = []
        self.vertices = {}

    def from_file(self, filename):
        """Read the data from a file and strip each \n."""

        if not os.path.isfile(filename):
            print 'Error: the file doesn\'t exist.'
            sys.exit(1)
        datafile = open(filename, 'r')
        for line in datafile.readlines():
            self.rawdata.append(line.strip('\n'))
        datafile.close()
        if not self.rawdata:
            print 'Error: the file is empty.'
            sys.exit(1)

    def from_data(self, rawdata):
        """Fill self.rawdata by rawdata."""

        if not rawdata:
            print 'Error: data is empty.'
            sys.exit(1)
        self.rawdata = rawdata

    def get_vertices(self):
        """Return the formated vertices."""

        return self.vertices

    def get_edges(self):
        """Return the formated edges."""

        return self.edges

    def edge_to_vertex(self, edges):
        """Convert a list of edges to a dictionnary of vertices."""

        vertices = {}
        for edge in edges:
            new_vertex = None
            if not edge[0] in vertices:
                new_vertex = edge[0]
            if str(new_vertex).isdigit():
                neighbour = []
                for edge in edges:
                    if new_vertex == edge[0]:
                        neighbour.append(edge[1])
                vertices.update({new_vertex: neighbour})
        return vertices


    def vertex_to_edge(self, vertices):
        """Convert a dictionnary of vertices to a list of edges."""

        edges = []
        for vertex in vertices:
            for neighbour in vertices[vertex]:
                edges.append((vertex, neighbour))
        return edges


class AdjacencyData(Data):
    """Adjacency data class formats adjacency matrix from a file."""

    def __init__(self):
        """self-explanatory"""

        Data.__init__(self)
        self.mytype = 'Adjacency'
        self.adjacency = None

    def from_file(self, filename):
        """self-explanatory"""

        Data.from_file(self, filename)
        self.__parse()

    def from_data(self, rawdata):
        """self-explanatory"""

        Data.from_data(self, rawdata)
        self.__parse()

    def __parse(self):
        """self-explanatory"""

        # We delete the first line if its contains the size of the matrix
        # because we dont need it
        if self.rawdata[0].isdigit():
            del self.rawdata[0]
        # The matrix cant be empty since we check that the file is not
        self.adjacency = [[value.isdigit() and int(value) or 0 \
                for value in row.split()] \
                for row in self.rawdata]
        self.vertices = self.adjacency_to_vertex(self.adjacency)
        self.edges = self.vertex_to_edge(self.vertices)

    def adjacency_to_vertex(self, adjacency):
        """Convert an adjacency matrix to a dictionnary of vertices."""

        vertices = {}
        if len(adjacency) != len(adjacency[0]):
            print 'Error: this is not a squarre matrix, please check the file.'
            sys.exit(1)
        for i in range(len(adjacency)):
            neighbour = []
            for j in range(len(adjacency)):
                if adjacency[i][j]:
                    neighbour.append(j)
            vertices.update({i: neighbour})
        return vertices
