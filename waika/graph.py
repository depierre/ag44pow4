#!/usr/bin/env python2


"""

        Graph file contains an implementation of graph structure as classes.


        Available features are create a graph, add a new vertex, delete one,
        test if a vertex is connected to another vertex.

"""


import sys
import os
import random
import time
from igraph import Graph
from igraph import plot
from igraph.drawing import colors
from copy import copy
from copy import deepcopy
from waika.parse import AdjacencyData


class GameGraph(object):
    """Graph class.
    This class allows a representation of a graph.

    """

    def __init__(self):
        """self-explanatory
        To create a graph, a dict is given containing a key representing the
        unique id of the vertex which has as value the list of connected
        vertices.

        Example: Complete graph K3
        {1: [2, 3], 2: [1, 3], 3: [1, 2]}

        """

        # Private attributs for the computation
        self.vertices = {}
        self.game_levels = []
        self.map_levels = {}
        self.longest_path = []
        # Private attributs for the graphical display
        self.sorted_vertices = []
        self.sorted_edges = []
        self.colors_scc = []
        # Private attributs for time of computation
        self.time_scc = 0
        self.time_matrix_N = 0
        self.time_longest_path = 0
        # Private attributs to know if the algorithms have been already applied
        # or not.
        self.done_scc = False
        self.done_matrix_N = False
        self.done_dag_longest_path = False
        self.done_longest_path = False

    def from_adjacency(self, filename):
        """Fill the graph from its adjacency matrix."""

        adjacency = AdjacencyData()
        adjacency.from_file(filename)
        self.vertices.update(adjacency.get_vertices())

    def from_data(self, data):
        """Fill the graph with formated data like vertices."""

        if not data:
            print 'Error: No vertex found.'
            print 'Maybe the previous graph has no SCCs.'
            sys.exit(1)
        self.vertices = data

    def tarjan_recursive(self):
        """Apply Tarjan's algorithm to find the strongly connected components
        of the graph.

        Returns a list of the strongly connected components.

        """

        stack = []
        index = {}
        lowlink = {}
        result_scc = {}

        def strongly_connected(vertex):
            """ Recursive application of the DFS algorithm to find the SCC of
            the graph.

            """

            index[vertex] = len(index)
            lowlink[vertex] = index[vertex]
            stack.append(vertex)

            # Consider successors of vertex
            for succ_vertex in self.vertices[vertex]:
                if succ_vertex not in index:
                    # Successor has not been yet visited, recurse ont it
                    strongly_connected(succ_vertex)
                    lowlink[vertex] = min(lowlink[succ_vertex], lowlink[vertex])
                elif succ_vertex in stack:
                    # Successor is in the stack and hence in the current SCC
                    lowlink[vertex] = min(lowlink[vertex], index[succ_vertex])

            # If vertex is a root one, pop the stack and generate an SCC
            if lowlink[vertex] == index[vertex]:
                scc = []
                succ_vertex = None
                while vertex != succ_vertex:
                    succ_vertex = stack.pop()
                    scc.append(succ_vertex)
                result_scc.update({len(result_scc): scc})

        time_start = time.clock()
        # We apply the DFS for each vertices of the graph
        for vertex in self.vertices:
            if not vertex in index:
                strongly_connected(vertex)
        self.time_scc = time.clock() - time_start
        if not self.done_scc:
            print '(time for SCCs:', self.time_scc, 's)'
        self.game_levels = result_scc
        self.done_scc = True

        return self.game_levels

    def direct_paths(self):
        """Compute the matrix of the direct paths between all
        the game levels.

        """

        if not self.done_scc:
            self.tarjan_recursive
        time_start = time.clock()
        # For each level we try to find a path with the next level
        for level in self.game_levels:
            current_edges = []
            for next_level in self.game_levels:
                # It's no sense to compute direct paths between a scc and
                # itself so the two levels have to be different.
                if level != next_level:
                    # And for each vertex from each level
                    for vertex in self.game_levels[level]:
                        for next_vertex in self.game_levels[next_level]:
                            # If there is a path between them so there is a
                            # path between the two SCCs.
                            if self.is_connected(vertex, next_vertex):
                                current_edges.append(next_level)
                    self.map_levels.update({level: current_edges})
        self.time_matrix_N = time.clock() - time_start
        if not self.done_matrix_N:
            print '(time for matrix N:', self.time_matrix_N, 's)'
        self.done_matrix_N = True

        return self.map_levels

    def get_no_income_edge(self, vertices):
        """Returns a list of all vertices that dont have income edge."""

        # We start by assuming that all the vertices dont have income edges
        no_income = [vertex for vertex in vertices]
        income = []
        for i in range(len(no_income)):
            for vertex in vertices:
                # If we find a edge from a vertex to our start vertex so
                # it doesn't have to be register as a start vertex
                if no_income[i] in vertices[vertex]:
                    income.append(no_income[i])
                    break

        return [i for i in range(len(no_income)) if i not in income]

    def top_order(self, vertices):
        """Topologically sort the graph and return a list of vertices as
        result.

        """

        sorted_vertices = []
        no_income = self.get_no_income_edge(vertices)

        while no_income:
            n = no_income.pop()
            sorted_vertices.append(n)
            while vertices[n]:
                m = vertices[n].pop()
                stop_vertex = True
                for vertex in vertices:
                    if m in vertices[vertex]:
                        stop_vertex = False
                        break
                if stop_vertex:
                    no_income.append(m)

        # If the graph still has edges so it has at least one cycle
        for vertex in vertices:
            if vertices[vertex]:
                return None

        return sorted_vertices

    def weight(self, start_vertex, stop_vertex):
        """Return the weight between start_vertex and stop_vertex."""

        current_weight = 0
        for neighbour in self.vertices[start_vertex]:
                if neighbour == stop_vertex:
                    current_weight = current_weight + 1

        return current_weight

    def find_dag_longest_path(self, entry_vertex = -1, exit_vertex = -1):
        """Find the longest path in a DAG."""

        if entry_vertex == -1:
            entry_vertex = min(self.vertices.keys())
        if exit_vertex == -1:
            exit_vertex = max(self.vertices.keys())

        time_start = time.clock()
        sorted_vertices = self.top_order(deepcopy(self.vertices))
        if not sorted_vertices:
            print 'Warning: this is not a DAG'
            return
        length_to = [0 for vertex in self.vertices]
        for v in sorted_vertices:
            for w in self.vertices[v]:
                temp_weight = self.weight(v, w)
                if length_to[w] < length_to[v] + temp_weight:
                    length_to[w] = length_to[v] + temp_weight
        self.time_dag_longest_path = time.clock() - time_start
        if not self.done_dag_longest_path:
            print '(time for dag longest path:', self.time_dag_longest_path, 's)'

        return max(length_to[v] for v in self.vertices)

    def find_longest_path(self, entry_vertex = -1, exit_vertex = -1):
        """Find the longest path, if it exists, between 'entry_vertex' and
        'exit_vertex'.

        Use a backtrack algorithm to find the longest path which means that
        all the paths between 'entry_point' and 'exit_point' are examinated.

        """

        if entry_vertex == -1:
            entry_vertex = min(self.vertices.keys())
        if exit_vertex == -1:
            exit_vertex = max(self.vertices.keys())

        def trackback(vertex, path):
            """Trackback algorithm to find all the possible paths."""

            # If we have found a Hamiltonian path, we dont need to look beyond.
            if len(self.longest_path) == len(self.vertices):
                return
            path.append(vertex)
            # We have reached the exit vertex
            if vertex == exit_vertex:
                # If the current path is longer than the actual
                # longest path we save it.
                if len(path) > len(self.longest_path):
                    self.longest_path = path
                    return
            else:
                successors = self.vertices[vertex]
                # We check if the current vertex has successors or not
                if not successors:
                    return
                else:
                    for successor in successors:
                        # If it's the first time that we cross the vertex,
                        # we continue the trackback
                        if successor not in path:
                            trackback(successor, copy(path))

        time_start = time.clock()
        trackback(entry_vertex, [])
        self.time_longest_path = time.clock() - time_start
        if not self.done_longest_path:
            print '(time for longest path:', self.time_longest_path, 's)'
        self.done_longest_path = True

        return self.longest_path

    def add_vertex(self, vertex=None):
        """Add a new vertex into the graph."""

        self.vertices.update({vertex['uid']: vertex['edges']})

    def del_vertex(self, uid=None):
        """Delete an existing vertex from the graph."""

        if uid and uid in self.vertices:
            # Delete the edges used by the vertex
            for vertex in self.vertices[uid]:
                self.vertices[vertex] = self.vertices[vertex].strip(uid)
            # Delete the vertex itself
            del self.vertices[uid]

    def is_connected(self, vertex, next_vertex):
        """Return true if there is an edge from vertex to neighbour."""

        if next_vertex in self.vertices[vertex]:
            return True
        return False

    def get_colors_scc(self):
        """Return the chosen colors for the SCCs."""

        return self.colors_scc

    def set_colors_scc(self, colors_scc):
        """Set the chosen colors for the SCCs."""

        self.colors_scc = colors_scc

    def __forced_computation(self):
        """Force the computation of attributs if they are not already computed."""

        if not self.done_scc:
            self.tarjan_recursive()
        if not self.done_matrix_N:
            self.direct_paths()
        if not self.done_longest_path:
            self.find_longest_path()

    def __map_to_matrix(self, map_levels):
        """Convert the dictionnary of map of the levels to a matrix."""

        matrix = [[0 for i in range(len(map_levels)) ] \
                for j in range(len(map_levels))]
        for i in range(len(map_levels)):
            nb_paths = 0
            for j in range(len(map_levels)):
                if i != j:
                    matrix[i][j] = map_levels[i].count(j)
        return matrix

    def display_console(self):
        """Console display with the SCC's, the possible ways between them and
        the longest path.

        """

        # Force computation if it's not already done
        self.__forced_computation()
        # Display the matrix of the graph
        print 'Vertices of the graph:'
        for vertex in self.vertices:
            print str(vertex) + ': ' + str(self.vertices[vertex])
        print
        # Display the map of the levels (i.e SCCs)
        print 'Map of the levels: ', self.game_levels
        print
        # Display the matrix of the direct paths between the levels
        print 'Matrix N of the direct paths:'
        matrix = self.__map_to_matrix(self.map_levels)
        for row in matrix:
            print row
        print
        # Display the longest path of the graph
        print 'Longest path:', self.longest_path
        print

    def display_graphical(self):
        """Graphical display of the graph with the strongly connected
        components and the longest path.

        """

        # Force computation if it's not already done
        self.__forced_computation()
        # Creation of a directed graph with as much vertices that the GameGraph
        g_display = Graph(len(self.vertices), directed=True)
        # Creation of a sorted list of the vertices and the edges of the graph
        edge_color = []
        vertex_shape = []
        vertex_color = {}
        game_level_color = {}

        for v in self.vertices:
            self.sorted_vertices.append(v)
            self.sorted_edges.extend([(v, w) for w in self.vertices[v]])
        # We add the edges to the graph
        for edge in self.sorted_edges:
            g_display.add_edges(edge)
        # We choose a different color for each SCC
        if not self.colors_scc:
            for level in range(len(self.game_levels)):
                game_level_color.update({level: \
                        random.choice(colors.known_colors.keys())})
            self.colors_scc = game_level_color
        else:
            game_level_color = self.colors_scc
        # We assign to each vertex its color
        for vertex in self.sorted_vertices:
            temp_color = ''
            for level in game_level_color:
                if vertex in self.game_levels[level]:
                    temp_color = game_level_color[level]
                    break
            vertex_color.update({vertex: temp_color})
        # We create a list of edges of the longest path of the graph
        longest_path_edges = []
        for i in range(len(self.longest_path) - 1):
            longest_path_edges.append((self.longest_path[i], \
                                       self.longest_path[i + 1]))
        # We assign two different colors: one for the longest path
        # and the second one for the others edges
        for edge in self.sorted_edges:
            if edge in longest_path_edges:
                edge_color.append('red')
            else:
                edge_color.append('silver')
        # Creation of the shapes of the vertices
        vertex_shape = ['rectangle']
        vertex_shape.extend(['circle' for v in range(len(self.vertices) - 2)])
        vertex_shape.extend(['rectangle'])
        # Creation of the layout of the graph
        # 'kk' is the short name for Kamada Kawai
        # (algorithm to display the graph)
        layout = g_display.layout("kk")
        # Creation of the visual style of the graph
        visual_style = {}
        visual_style["vertex_label"] = self.sorted_vertices
        visual_style["layout"] = layout
        visual_style["vertex_color"] = [vertex_color[i] \
                for i in range(len(vertex_color))]
        visual_style["edge_color"] = edge_color
        visual_style["vertex_shape"] = vertex_shape
        visual_style["bbox"] = (1600, 900)
        visual_style["margin"] = 20
        # Graphical draw of the graph
        plot(g_display, **visual_style)
