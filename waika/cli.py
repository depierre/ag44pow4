#!/usr/bin/env python2


"""

        CLI file contains the Command Line Interface.


        With the cli, the user is able to apply the different algorithms to any
        file containing the adjacency matrix of the graph.

"""


import os
import sys
from waika.graph import GameGraph


def parse(args):
    """Parse the arguments.
    'args' must contains a list of filename with an adjacency matrix of a graph
    as data.

    """

    for arg in args:
        if os.path.isfile(arg):
            graph = GameGraph()
            graph.from_adjacency(arg)
            graph.tarjan_recursive()
            graph_matrix_N = GameGraph()
            map_levels = graph.direct_paths()
            graph_matrix_N.from_data(map_levels)
            graph.find_longest_path()

            graph.display_console()
            graph.display_graphical()

            colors_scc = graph.get_colors_scc()
            weight_longest_path = graph_matrix_N.find_dag_longest_path()
            print 'Weight of the longest path:', weight_longest_path
            graph_matrix_N.set_colors_scc(colors_scc)
            graph_matrix_N.display_graphical()
        else:
            print 'Error:', arg, 'doesn\'t exist.'
            sys.exit(1)
