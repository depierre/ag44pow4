Ariadne's thread.
================


Author: Tao "DePierre" Sauvage
Creation: 10/16/2012


This project is provided by the UTBM for students following lecture AG44
(Graph Theory).


The set of rules is available into the PDF file. We have to manage files
which contain a binary matrix. This last one represents the possible
solutions to go from one level to another one.
If [i, j] equals to 1 so there is a way to go from level i to the j one.


Our goal is to provide an algorithm to answer such problem.


The Python language is used here and the project is under WTFPL license.
